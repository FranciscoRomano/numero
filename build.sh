#!/bin/bash
#export MONGO_URL=mongodb://__USERNAME__:__PASSWORD__@__IP__:__PORT__/__DATABASE__
#meteor build $VAR_BINARY_DIR --server __WEBSITE__
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ~~ ANDROID SETUP ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#developer.indieboppers@gmail.com
# WARN :: You may need to call this line before building:
#
#         keytool -genkey -alias <app-name> -keyalg RSA -keysize 2048 -validity 8760

VAR_NAME="Numero"
#VAR_PASSWORD="IndieBoppers01"
VAR_PASSWORD="indieboppers01"
VAR_BINARY_DIR="$(pwd)/../ANDROID-build"

echo "=================================================="
echo " { BUILD PROCESS 1/3 }\n"
# build application
rm -rf "$VAR_BINARY_DIR/android"
meteor build $VAR_BINARY_DIR --server locahost:3000
cp "$VAR_BINARY_DIR/android/project/build/outputs/apk/release/android-release-unsigned.apk" "$VAR_BINARY_DIR/android"
mv "$VAR_BINARY_DIR/android/android-release-unsigned.apk" "$VAR_BINARY_DIR/android/release-unsigned.apk"

echo "=================================================="
echo " { BUILD PROCESS 2/3 }\n"
# sign application apk
jarsigner -verbose -keystore ".keystore" -storepass "$VAR_PASSWORD" -sigalg SHA1withRSA -digestalg SHA1 "$VAR_BINARY_DIR/android/release-unsigned.apk" "$VAR_NAME"

echo "=================================================="
echo " { BUILD PROCESS 3/3 }\n"
# optimize application apk
cd "$VAR_BINARY_DIR/android"
$ANDROID_HOME/build-tools/25.0.2/zipalign 4 "release-unsigned.apk" "$VAR_NAME.apk"

echo "================================================="
echo " { BUILD COMPLETE }"
