/** Dependencies **********************************************************************************/
/** Declarations **********************************************************************************/

export var Random = {};
// global variables

// get random value
Random.Get = function()
{
    return Math.random();
}

// get random value from range
Random.GetRange = function(min, max)
{
    return Math.random() * (max - min) + min;
};

/**************************************************************************************************/