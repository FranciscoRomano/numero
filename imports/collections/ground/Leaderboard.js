/** Dependencies **********************************************************************************/

import { GroundX } from '/imports/classes.js'

/** Declarations **********************************************************************************/

export var Leaderboard = GroundX.Collection('gd_leaderboard');

// add document to leaderboard
GroundX.Function('add_leaderboard', function(json)
{
    let shouldInsert = false;
    // count documents
    let count = Leaderboard.find().fetch().length;
    // check current length
    if (count > 4)
    {
        let index = 0;
        // sort documents by moves
        let list = Leaderboard.find({ moves: { $gt: json.moves } }).fetch();
        // check if list contains elements
        if (list.length > 1)
        {
            // get lowest index
            for (let i = 1; i < list.length; i++)
            {
                if (list[i].moves > list[index].moves)
                {
                    index = i;
                }
            }
            // remove document
            shouldInsert = true;
            Leaderboard.remove({ _id: list[index]._id });
        }
    }
    else shouldInsert = true;
    // insert new document
    if (shouldInsert) Leaderboard.insert(json);
});

// get documents from leaderboard
GroundX.Function('get_leaderboard', function(json)
{
    let list = Leaderboard.find().fetch();
    // sort list in ascending order
    for (let i = 0; i < list.length; i++)
    {
        for (let ii = i + 1; ii < list.length; ii++)
        {
            if (list[ii].moves < list[i].moves)
            {
                // swap values
                let temp = list[i];
                list[i] = list[ii];
                list[ii] = temp;
            }
        }
    }
    // return ordered list
    return list;
});

// clear documents from leaderboard
GroundX.Function('clear_leaderboard', function(json)
{
    let list = Leaderboard.find().fetch();
    // iterate through list
    list.forEach(function(element)
    {
        // remove document by id
        Leaderboard.remove({ _id: element._id });
    });
});

/**************************************************************************************************/