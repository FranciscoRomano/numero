/** Dependencies **********************************************************************************/
/** Declarations **********************************************************************************/

export var GroundX = {};
GroundX._functions = [];
GroundX._collections = [];

// run function
GroundX.Run = function(name, json)
{
    // execute function
    return this._functions[name](json);
}

// call function
GroundX.Call = function(name, json, callback)
{
    window.setTimeout(function()
    {  
        try
        {
            // try executing function
            callback(null, this._functions[name](json));
        }
        catch(e)
        {
            // send error message to callback
            console.error('MongoX :: Error, ' + e || e.message);
        }
    }, 0);
}

// create or fetch functions
GroundX.Function = function(name, method)
{
    // check if client
    if (!Meteor.isClient) return null;
    // check if function exist
    if (!this._functions[name])
    {
        // set new function
        this._functions[name] = method;
    }
    else console.warn('GroundX :: function "' + name + '" already exists!');
    // return function
    return this._functions[name];
};

// create or fetch collections
GroundX.Collection = function(name)
{
    // check if client
    if (!Meteor.isClient) return null;
    // check if collection exists
    if (!this._collections[name])
    {
        // set new collection
        this._collections[name] = new Ground.Collection(name);
    }
    else console.warn('GroundX :: collection "' + name + '" already exists!');
    // return collection
    return this._collections[name];
}

/**************************************************************************************************/