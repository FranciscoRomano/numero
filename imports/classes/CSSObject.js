/** Dependencies **********************************************************************************/
/** Declarations **********************************************************************************/

export class CSSObject
{
    constructor(element)
    {
        this.m_elem = element;
    }
    get(name)
    {
        let value = this.m_elem.style[name];
        return value || window.getComputedStyle(this.m_elem, null)[name];
    }
    set(name, value)
    {
        this.m_elem.style[name] = value;
    }
}

/**************************************************************************************************/