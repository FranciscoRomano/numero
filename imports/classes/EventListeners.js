/** Dependencies **********************************************************************************/
/** Declarations **********************************************************************************/

export var EventListeners = {};
// global variables
EventListeners._listeners = [];

// add event listener to target
EventListeners.Add = function(target, type, method, options = false)
{
    if (typeof target == "string") target = document.querySelector(target);
    // register listener
    this._listeners.push({
        "type": type,
        "method": method,
        "target": target,
        "options": options
    });
    // add event listener
    target.addEventListener(type, method, options);
};

// remove event listener on target
EventListeners.Remove = function(target, type)
{
    if (typeof target == "string") target = document.querySelector(target);
    // iterate through listeners
    for (let i = 0; i < this._listeners.length; i++)
    {
        let info = this._listeners[i];
        // check listener target
        if (info.target == target)
        {
            // check listener type
            if (info.type == type)
            {
                // remove event listener
                info.target.removeEventListener(info.type, info.method, info.options);
                return this._listeners.splice(i, 1);
            }
        }
    };
};

// remove all event listeners on target
EventListeners.RemoveAll = function(target, type = false)
{
    if (typeof target == "string") target = document.querySelector(target);
    // iterate through listeners
    for (let i = 0; i < this._listeners.length; i++)
    {
        let info = this._listeners[i];
        // check listener target
        if (info.target == target)
        {
            // check listener type
            if (type == false || info.type == type)
            {
                // remove event listener
                info.target.removeEventListener(info.type, info.method, info.options);
                this._listeners.splice(i, 1);
                i--;
            }
        }
    };
};

/**************************************************************************************************/