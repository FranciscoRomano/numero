/** Dependencies **********************************************************************************/

// https://easings.net/
// https://s3.amazonaws.com/blog.invisionapp.com/uploads/2015/04/Interaction.gif

import { EventListeners } from "../../../imports/classes/EventListeners";

/** Declarations **********************************************************************************/

export class action_button
{
    // default constructor
    constructor(target)
    {
        if (typeof target == "string") target = document.querySelector(target);
        // set variables
        this.m_visible = true;
        this.m_element = target;
        this.m_elements = target.querySelectorAll(".option");
        // set default has closed
        this.close();
    }
    // set current elements has opened
    open()
    {
        let delta = Math.PI / (this.m_elements.length - 1);
        this.m_element.className += " show";
        for (let i = 0; i < this.m_elements.length; i++)
        {
            let y = Math.round(Math.sin(delta * i) * 72);
            let x = Math.round(1 - Math.cos(delta * i) * 72);
            this.m_elements[i].className += " show";
            this.m_elements[i].style.top = "calc(50% - " + y + "px)";
            this.m_elements[i].style.left = "calc(50% + " + x + "px)";
        }
    }
    // set current elements has closed
    close()
    {
        this.m_element.className = this.m_element.className.split(" show").join("");
        for (let i = 0; i < this.m_elements.length; i++)
        {
            this.m_elements[i].className = this.m_elements[i].className.split(" show").join("");
            this.m_elements[i].style.top = "50%";
            this.m_elements[i].style.left = "50%";
        }
    }
    // get element within the given radius
    elem_in_radius(x, y, radius)
    {
        let rect;
        let dirX;
        let dirY;
        // iterate through elements
        for (let i = 0; i < this.m_elements.length; i++)
        {
            // calculate direction
            rect = this.m_elements[i].getBoundingClientRect();
            dirY = y - (rect.y + rect.height / 2);
            dirX = x - (rect.x + rect.width / 2);
            // validate minimum radius length
            if (Math.sqrt(dirX * dirX + dirY * dirY) < radius)
                return this.m_elements[i];
        }

        return null;
    }
}


/**************************************************************************************************/