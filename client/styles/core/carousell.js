/** Dependencies **********************************************************************************/
/** Declarations **********************************************************************************/

// fixes required:
// - have 3 classes only for transitions [left, center, right]
// - make every element left of the index stay left (use same classes)
// - make every element right of the index stay right (use same classes)

export class carousell
{
    // default constructor
    constructor(target)
    {
        if (typeof target == "string") target = document.querySelector(target);
        // set variables
        this.m_index = 0;
        this.m_elements = target.children;
        this.m_classList = [ "inactive-l", "active", "inactive-r" ];
        // prepair carousell elements
        this.reset_to(0);
    }
    // move carousell elements left
    move_l()
    {
        return this.reset_to(this.m_index - 1);
    }
    // move carousell elements right
    move_r()
    {
        return this.reset_to(this.m_index + 1);
    }
    // mirror carousell current index
    mirror()
    {
        this.reset_to(this.m_elements.length - this.m_index - 1);
    }
    // length of the carousell elements
    length()
    {
        return this.m_elements.length;
    }
    // reset carousell elements to index
    reset_to(index)
    {
        if (index > -1 && index < this.m_elements.length)
        {
            let elem;
            let className;
            // iterater through each element
            for (let i = 0; i < this.m_elements.length; i++)
            {
                elem = this.m_elements[i];
                // remove old classes by index
                className = " " + this.m_classList[(i < this.m_index ? 0 : i == this.m_index ? 1 : 2)];
                elem.className = elem.className.replace(className, "");
                // append new classes by index
                className = " " + this.m_classList[(i < index ? 0 : i == index ? 1 : 2)];
                elem.className += className;
            }
            // update current index
            this.m_index = index;
            return true;
        }
        return false;
    }   
}


/**************************************************************************************************/