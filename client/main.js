/** Dependencies **********************************************************************************/

// ground db percistent
// adds - google adds
// fix ui, make it less frustating,
// come up with a new design for the operations

// Use the following link to know more about material icons:
// https://material.io/icons/

import { Game          } from './scripts/Game.js'
import { TileSlider    } from './scripts/TileSlider.js';
import { Operator      } from './scripts/Operator.js';
import { GameManager   } from './scripts/GameManager.js';
import { OperatorDock  } from './scripts/OperatorDock.js';

import { Meteor      } from 'meteor/meteor';
import { GroundX     } from '/imports/classes.js';
import { Leaderboard } from '/imports/collections.js';

import { Random         } from '/imports/Random.js';          // basic random class with ranges
import { EventListeners } from "/imports/classes.js"; // Managing event listeners made easy
import { MenuManager }    from './scripts/MenuManager.js';

/** Declarations **********************************************************************************/

// disable operators when moving 1
// reset all operators when switching pages

globals = {
    
    gameDock: null,
    gameOpAdd: null,
    gameOpSub: null,
    gameOpMul: null,
    gameOpDiv: null,
    gameTileSlider: null,
    gameTileRandoms: [],



    isRandomGame: false,
    isCustomGame: false,
    isSliderGame: false,

    gamemanager: null,
};
g_EventListeners = EventListeners;


PerformanceTest = { _startTime: {} };
function BeginPerformanceTest(name) { PerformanceTest._startTime[name] = performance.now(); }
function EndPerformanceTest(name) { const duration = performance.now() - PerformanceTest._startTime[name]; console.log("Performance Test:: '" + name + "' took { " + (duration / 1000) + " }"); }


function ResetGame()
{
    globals.gameDock.reset();
    globals.gameOpAdd.reset();
    globals.gameOpSub.reset();
    globals.gameOpMul.reset();
    globals.gameOpDiv.reset();
}

function ResetMenu()
{
}

function PrepairGame()
{
    ResetGame();
    globals.gameOpAdd.enable();
    globals.gameOpSub.enable();
    globals.gameOpMul.enable();
    globals.gameOpDiv.enable();
}

function PrepairMenu()
{
    ResetMenu();
}

function BackToGamePage()
{
    MenuManager.Hide();
    // reset menu
    ResetMenu();
    // prepair game
    PrepairGame();
}

function BackToMenuPage()
{
    globals.isRandomGame = false;
    globals.isCustomGame = false;
    globals.isSliderGame = false;
    MenuManager.Show();
    // reset game
    ResetGame();
    // prepair menu
    PrepairMenu();
}

function CreateRandomGame()
{
    // create random game
    globals.gamemanager.SetTargetRange(-500, 500);
    globals.gamemanager.SetRandomRange( -10,  10);
    globals.gamemanager.SetNumberRange(   1,  10);
    globals.gamemanager.Create();
    globals.isRandomGame = true;
}

function CreateSliderGame()
{
    // create slider game
    globals.gamemanager.SetTargetRange(-500, 500);
    globals.gamemanager.SetRandomRange( -10,  10);
    globals.gamemanager.SetNumberRange(   1,  10);
    globals.gamemanager.Create();
    globals.isSliderGame = true;
    // prepair random values
    let values, target = 999;
    while (target < -255 || target > 255)
    {
        values = [];
        target = globals.gamemanager.GetNumberValue();
        for (let i = 0; i < 5; i++)
        {
            let random = 0;
            while (random == 0) random = globals.gamemanager.GenerateFromRange(-10, 10);
            values.push(random);
            switch(Math.floor(Math.random() * 3))
            {
                case 0:
                    target += values[i];
                    break;
                case 1:
                    target -= values[i];
                    break;
                case 2:
                    target *= values[i];
                    break;
                case 3:
                    target /= values[i];
                    break;
            }
        }
    }
    globals.gamemanager._game.setTarget(target);
    globals.gamemanager.SetRandomList(values)
}

function CreateCustomGame(target)
{
    // create custom game
    globals.gamemanager.SetTargetRange(target, target);
    globals.gamemanager.SetRandomRange(   -10,     10);
    globals.gamemanager.SetNumberRange(     1,     10);
    globals.gamemanager.Create();
    globals.isCustomGame = true;
}

function ValidateCurrentGame()
{
    // check if game finished
    if (globals.gamemanager.IsComplete())
    {
        // dock glow
        globals.gameDock.glow();
        // remove event listeners
        globals.gameOpAdd.reset();
        globals.gameOpSub.reset();
        globals.gameOpMul.reset();
        globals.gameOpDiv.reset();
        // add new event listeners
        EventListeners.Add(globals.gameDock.m_icon, "click", function()
        {
            PrepairGame();
            if (globals.isSliderGame)
            {
                CreateSliderGame();
            }
            else
            {
                globals.gamemanager.Create();
            }
            EventListeners.Remove(globals.gameDock.m_icon, "click");
            // else BackToMenuPage();
        }, { passive: true });
    }
    else PrepairGame();
}

// ###############################################
// ## Handle Game Operators ######################
// ###############################################

function HandleGameOperatorEnd(operator)
{
    // prepair game
    PrepairGame();
}

function HandleGameOperatorBegin(operator)
{
    // disable extra listeners
    if (operator.id() != globals.gameOpAdd.id()) globals.gameOpAdd.reset();
    if (operator.id() != globals.gameOpSub.id()) globals.gameOpSub.reset();
    if (operator.id() != globals.gameOpMul.id()) globals.gameOpMul.reset();
    if (operator.id() != globals.gameOpDiv.id()) globals.gameOpDiv.reset();
}

function HandleGameOperatorSuccess(operator)
{
    // check game state
    if (!globals.gamemanager.IsComplete())
    {
        // check operation type
        switch(operator.id())
        {
            case "add":
                globals.gamemanager.ApplyAdd();
                break;
            case "sub":
                globals.gamemanager.ApplySub();
                break;
            case "mul":
                globals.gamemanager.ApplyMul();
                break;
            case "div":
                globals.gamemanager.ApplyDiv();
                break;
        }
    }
    // reset dock & operator
    HandleGameOperatorEnd(operator);
    // validate game
    ValidateCurrentGame();
}

// ###############################################
// ## Initialize #################################
// ###############################################

Meteor.startup(function()
{
    console.warn('METEOR :: touch events fail to work after long periods of time');
    
    // initialize managers
    MenuManager.Init();
    GameManager.Init();
    
    // prepair game page
    //globals.gameDock        = new OperatorDock(".layout-page.game .operator-dock");
    //globals.gameOpAdd       = new Operator(".layout-page.game .operator#add", globals.gameDock, HandleGameOperatorBegin, HandleGameOperatorSuccess, HandleGameOperatorEnd);
    //globals.gameOpSub       = new Operator(".layout-page.game .operator#sub", globals.gameDock, HandleGameOperatorBegin, HandleGameOperatorSuccess, HandleGameOperatorEnd);
    //globals.gameOpMul       = new Operator(".layout-page.game .operator#mul", globals.gameDock, HandleGameOperatorBegin, HandleGameOperatorSuccess, HandleGameOperatorEnd);
    //globals.gameOpDiv       = new Operator(".layout-page.game .operator#div", globals.gameDock, HandleGameOperatorBegin, HandleGameOperatorSuccess, HandleGameOperatorEnd);
    //globals.gamemanager     = new GameManager(new Game());
    //globals.gameTileRandoms = document.querySelectorAll(".layout-page.game .screen .tile-random-list .tile");
    
    //BackToMenuPage();
    BackToGamePage();
    
    EventListeners.Add(".menu button.random", "click", function(e)
    {
        BackToGamePage();
        //CreateRandomGame();
    });

    EventListeners.Add(".menu button.custom", "click", function(e)
    {
        BackToGamePage();
        //CreateCustomGame(50);
    });

    EventListeners.Add(".menu button.slider", "click", function(e)
    {
        BackToGamePage();
        //CreateSliderGame();
    });
    // document.querySelector(".popup-slider input").oninput = function()
    // {
    //     document.querySelector(".popup-slider .number").innerHTML = 'Target: ' + this.value;
    // }
    
    // elem_leaderboard = document.querySelector('.page.menu .leaderboard');
    //UpdateLeaderboard();

    EventListeners.Add(document.querySelector(".layout-page.game .title i"), 'click', BackToMenuPage);
});

/**************************************************************************************************/