/** Dependencies **********************************************************************************/

import { TileSlider } from './TileSlider.js';

/** Declarations **********************************************************************************/

export class Game
{
    constructor()
    {
        this.m_elements = {
            page:       document.querySelector(".page.game"),
            target:     document.querySelector(".page.game .target"),
            number:     document.querySelector(".page.game .number"),
            counter:    document.querySelector(".page.game .counter"),
            tileSlider: new TileSlider(),
        };
        this.m_information = {
            count: 0,
            number: 0,
            target: 0,
        };
    }
    reset()
    {
        this.setNumber(0);
        this.setRandom(0);
        this.setTarget(0);
        this.setCounter(0);
    }
    applyAdd()
    {
        this.setCounter(this.m_information.count + 1);
        this.setNumber(this.m_information.number + this.m_elements.tileSlider.GetValue());
    }
    applySub()
    {
        this.setCounter(this.m_information.count + 1);
        this.setNumber(this.m_information.number - this.m_elements.tileSlider.GetValue());
    }
    applyMul()
    {
        this.setCounter(this.m_information.count + 1);
        this.setNumber(this.m_information.number * this.m_elements.tileSlider.GetValue());
    }
    applyDiv()
    {
        this.setCounter(this.m_information.count + 1);
        this.setNumber(this.m_information.number / this.m_elements.tileSlider.GetValue());
    }
    getNumber() { return this.m_information.number; }
    setNumber(value)
    {
        value = Math.max(-999, Math.min(999, Math.round(value)));
        this.m_information.number = value;
        this.m_elements.number.innerHTML = value;
    }
    getRandom() { return this.m_elements.tileSlider.GetValue(); }
    setRandom(value)
    {
        value = Math.max(-999, Math.min(999, Math.round(value)));
        this.m_elements.tileSlider.Prepair([value]);
    }
    getTarget() { return this.m_information.target; }
    setTarget(value)
    {
        value = Math.max(-999, Math.min(999, Math.round(value)));
        this.m_information.target = value;
        this.m_elements.target.innerHTML = value;
    }
    getCounter() { return this.m_information.count; }
    setCounter(value)
    {
        value = Math.round(value);
        this.m_information.count = value;
        this.m_elements.counter.innerHTML = "Moves: " + value;
    }
    nextRandom()
    {
        this.m_elements.tileSlider.RemoveValue();
        return this.m_elements.tileSlider.Length() != 0;
    }
    setRandomList(values)
    {
        this.m_elements.tileSlider.Prepair(values);
    }
    generateFromRange(min, max)
    {
        return Math.max(-999, Math.min(999, Math.floor(Math.random() * (max - min) + min)));
    }
    setNumberFromRange(min, max)
    {
        this.setNumber(Math.random() * (max - min) + min);
    }
    setRandomFromRange(min, max)
    {
        this.setRandom(Math.random() * (max - min) + min);
    }
    setTargetFromRange(min, max)
    {
        this.setTarget(Math.random() * (max - min) + min);
    }
}

/**************************************************************************************************/