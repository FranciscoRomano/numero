/** Dependencies **********************************************************************************/

import { Mathf } from "/imports/classes/Mathf.js";
import { CSSObject } from "/imports/classes/CSSObject.js";
import { EventListeners } from "/imports/classes/EventListeners.js";

/** Declarations **********************************************************************************/

export class Operator
{
    constructor(selectors, dock, beginCallback, successCallback, endCallback)
    {
        this.m_dock            = dock;
        this.m_elem            = document.querySelector(selectors);
        this.m_class           = this.m_elem.className;
        this.m_touch           = { offset: { x: 0, y: 0 }, origin: { x: 0, y: 0 } };
        this.m_elemId          = this.m_elem.id;
        this.m_elemCSS         = new CSSObject(this.m_elem);
        this.m_endCallback     = endCallback;
        this.m_beginCallback   = beginCallback;
        this.m_successCallback = successCallback;
        // public variables
        this.position = {
            y: this.m_elemCSS.get("top").replace("px",""),
            x: this.m_elemCSS.get("left").replace("px","")
        };
        // map current object
        Operator.MAP[this.m_elemId] = this;
        // add event listeners
        this._AddListener("touchstart");
    }
    id() { return this.m_elemId; }
    dock() { this.m_elem.className = this.m_class + " dock"; }
    pulse() { this.m_elem.className = this.m_class + " pulse"; }
    reset()
    {
        this.m_elem.className  = this.m_class;
        this.m_elem.style.top  = null;
        this.m_elem.style.left = null;
        this.m_elem.style.opacity = null;
        this.m_dock.m_elem.style.opacity = null;
        this._RmvListener("touchend");
        this._RmvListener("touchmove");
        this._RmvListener("touchstart");
        //document.documentElement.style.setProperty("--var-operatorAnimation-alpha", "1.0");
    }
    enable() { this._AddListener("touchstart"); }
    calcDistance()
    {
        // calculate distance
        let length01 = {
            x: (this.m_dock.position.x - this.m_touch.origin.x),
            y: (this.m_dock.position.y - this.m_touch.origin.y),
        };
        length01 = Math.sqrt(length01.x * length01.x + length01.y * length01.y);
        let vec02 = {
            x: (this.m_dock.position.x - this.position.x),
            y: (this.m_dock.position.y - this.position.y),
        };
        return Math.sqrt(vec02.x * vec02.x + vec02.y * vec02.y) / length01;
    }
    _RmvListener(name) { EventListeners.RemoveAll(this.m_elem, name); }
    _AddListener(name)
    {
        switch(name)
        {
            case "touchend":
                return EventListeners.Add(this.m_elem, name, function(e) { Operator.MAP[e.target.id]._HandleTouchEnd(e); }, { passive: true });
            case "touchmove":
                return EventListeners.Add(this.m_elem, name, function(e) { Operator.MAP[e.target.id]._HandleTouchMove(e); }, { passive: true });
            case "touchstart":
                return EventListeners.Add(this.m_elem, name, function(e) { Operator.MAP[e.target.id]._HandleTouchStart(e); }, { passive: true });
        }
    }
    _HandleTouchEnd(e)
    {
        // calculate distance
        let distance = this.calcDistance();
        // check for valid distance
        if (distance < 0.7)
        {
            this.dock();
            window.setTimeout(this.m_successCallback, 200, this);
        }
        else window.setTimeout(this.m_endCallback, 0, this);
    }
    _HandleTouchMove(e)
    {
        // set variables
        this.position.y = e.touches[0].clientY;
        this.position.x = e.touches[0].clientX;
        this.m_elem.style.top =  (this.m_touch.offset.y + this.position.y) + 'px';
        this.m_elem.style.left = (this.m_touch.offset.x + this.position.x) + 'px';
        // calculate distance
        let distance = this.calcDistance();
        // set pulse speed of operator and dock
        // document.documentElement.style.setProperty(
        //     "--var-operatorAnimation-alpha",
        //     "" + Mathf.Lerp(0.5, 1.0, distance)
        // );
        //document.documentElement.style.setProperty("--var-pulseAnimation-alpha", 0.2);
        //this.m_elem.style.opacity = pulse;
        //this.m_dock.m_elem.style.opacity = pulse;
    }
    _HandleTouchStart(e)
    {
        // animate
        this.pulse();
        this.m_dock.pulse();
        this._HandleTouchMove(e);
        this.m_beginCallback(this);
        // set variables
        this.m_touch.origin.x = e.touches[0].clientX;
        this.m_touch.origin.y = e.touches[0].clientY;
        this.m_touch.offset.y = this.m_elemCSS.get("top").replace("px", "")  - e.touches[0].clientY;
        this.m_touch.offset.x = this.m_elemCSS.get("left").replace("px", "") - e.touches[0].clientX;
        // remove event listeners
        this._RmvListener("touchstart");
        // add new event listeners
        this._AddListener("touchend");
        this._AddListener("touchmove");
    }
}
Operator.MAP = {};

/**************************************************************************************************/