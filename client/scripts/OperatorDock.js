/** Dependencies **********************************************************************************/

import { CSSObject } from "/imports/classes/CSSObject.js";

/** Declarations **********************************************************************************/

export class OperatorDock
{
    constructor(selectors)
    {
        this.m_elem = document.querySelector(selectors);
        this.m_icon = document.querySelector(selectors + " i");
        this.m_radius = new CSSObject(this.m_elem).get("width").replace("px","") / 2;
        // public variables
        this.position = {
            y: new CSSObject(this.m_elem).get("top").replace("px", ""),
            x: new CSSObject(this.m_elem).get("left").replace("px", "")
        };
    }
    glow()
    {
        this.m_elem.className = "operator-dock glow";
        if (this.m_icon) this.m_icon.className = "material-icons show";
    }
    reset()
    {
        this.m_elem.className = "operator-dock";
        if (this.m_icon) this.m_icon.className = "material-icons";
    }
    pulse()
    {
        this.m_elem.className = "operator-dock pulse";
        if (this.m_icon) this.m_icon.className = "material-icons";
    }
}

/**************************************************************************************************/