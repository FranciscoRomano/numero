/** Dependencies **********************************************************************************/

import { carousell } from '../styles/core/carousell';
import { EventListeners } from '../../imports/classes/EventListeners';

function _ResetEventListener(target, type, callback)
{
    EventListeners.RemoveAll(target, type);
    window.setTimeout(function() {
        EventListeners.Add(target, type, callback);
    }, 200);
}

function MenuManager_ClickL(e)
{
    // iterate through each carousell item
    for (let i = 0; i < MenuManager.m_carousells.length; i++)
    {
        MenuManager.m_carousells[i].move_l();
    }
    // reset current event listener
    _ResetEventListener(".layout-page.menu .carousell_button.left", "click", MenuManager_ClickL);
}

function MenuManager_ClickR(e)
{
    // iterate through each carousell item
    for (let i = 0; i < MenuManager.m_carousells.length; i++)
    {
        MenuManager.m_carousells[i].move_r();
    }
    // reset current event listener
    _ResetEventListener(".layout-page.menu .carousell_button.right", "click", MenuManager_ClickR);
}

function MenuManager_ClickPin(e)
{
    let elem = e.target;
    // check if inactive pin
    if (elem.className.search("inactive") > -1)
    {
        // reset carousells to index
        for (let i = 0; i < MenuManager.m_carousells.length; i++)
        {
            MenuManager.m_carousells[i].reset_to(elem.innerHTML - 1);
        }
        // reset current event listener
        _ResetEventListener(e.target, "click", MenuManager_ClickPin);
    }
}

/** Declarations **********************************************************************************/

export let MenuManager = {};

// initialize menu manager
MenuManager.Init = function()
{
    // set variables
    MenuManager.m_element = document.querySelector(".layout-page.menu");
    MenuManager.m_carousells = [
        new carousell(MenuManager.m_element.querySelector(".carousell_pins")),
        new carousell(MenuManager.m_element.querySelector(".carousell_pages")),
    ];
    // add required event listeners
    EventListeners.Add(MenuManager.m_element.querySelector(".carousell_button.left"), "click", MenuManager_ClickL);
    EventListeners.Add(MenuManager.m_element.querySelector(".carousell_button.right"), "click", MenuManager_ClickR);
    EventListeners.Add(MenuManager.m_element.querySelectorAll(".carousell_pins .pin")[0], "click", MenuManager_ClickPin);
    EventListeners.Add(MenuManager.m_element.querySelectorAll(".carousell_pins .pin")[1], "click", MenuManager_ClickPin);
    EventListeners.Add(MenuManager.m_element.querySelectorAll(".carousell_pins .pin")[2], "click", MenuManager_ClickPin);
    EventListeners.Add(MenuManager.m_element.querySelectorAll(".carousell_pins .pin")[3], "click", MenuManager_ClickPin);
    EventListeners.Add(MenuManager.m_element.querySelectorAll(".carousell_pins .pin")[4], "click", MenuManager_ClickPin);
};

// set menu element has show
MenuManager.Show = function(callback)
{
    // set menu class
    MenuManager.m_element.className = "layout-page menu";
    // execute callback
    if (callback) window.setTimeout(callback, 150);
};

// set menu element has hide
MenuManager.Hide = function(callback)
{
    // set menu class
    MenuManager.m_element.className = "layout-page menu hide";
    // execute callback
    if (callback) window.setTimeout(callback, 150);
};

/**************************************************************************************************/