/** Dependencies **********************************************************************************/

import { action_button } from "../styles/core/action_button";
import { EventListeners } from "../../imports/classes/EventListeners";

let g_action_element = null;
let g_action_pressed = false;

function _ShowActions()
{
    GameManager.m_actionButton.open();
}

function _HideActions()
{
    GameManager.m_actionButton.close();
}

function _HandleActionClick(e)
{
    g_action_pressed = !g_action_pressed;
    (g_action_pressed ? _ShowActions : _HideActions)();
}

function _HandleActionTouchEnd(e)
{
    _HideActions();
    console.log(g_action_element);
}

function _HandleActionTouchMove(e)
{
    let touch = e.touches[0];
    // reset class name
    if (g_action_element) g_action_element.className = g_action_element.className.split(" hover").join("");
    // get closest element
    g_action_element = GameManager.m_actionButton.elem_in_radius(touch.clientX, touch.clientY, 18);
    if (g_action_element) g_action_element.className += " hover"
}

function _HandleActionTouchStart(e)
{
    _ShowActions();
}

/** Declarations **********************************************************************************/

export let GameManager = {};

// initialize game manager
GameManager.Init = function()
{
    // set variables
    GameManager.m_element = document.querySelector(".layout-page.game");
    GameManager.m_actionButton = new action_button(GameManager.m_element.querySelector(".action_button_game"));
    // add required event listeners
    EventListeners.Add(GameManager.m_element.querySelector(".action_button_game i"), "touchend", _HandleActionTouchEnd);
    EventListeners.Add(GameManager.m_element.querySelector(".action_button_game i"), "touchmove", _HandleActionTouchMove);
    EventListeners.Add(GameManager.m_element.querySelector(".action_button_game i"), "touchstart", _HandleActionTouchStart);
    //EventListeners.
};

// export class GameManager
// {
//     constructor(game)
//     {
//         this._game = game,
//         this._timer = new Date();
//         this._isRunning = false;
//         this._randomList = [];
//         this._numberRange = { min: 0, max: 0 };
//         this._randomRange = { min: 0, max: 0 };
//         this._targetRange = { min: 0, max: 0 };
//     }
//     Create()
//     {
//         this._game.reset();
//         this._game.setNumberFromRange(this._numberRange.min, this._numberRange.max);
//         this._game.setRandomFromRange(this._randomRange.min, this._randomRange.max);
//         this._game.setTargetFromRange(this._targetRange.min, this._targetRange.max);
//         this._timer = new Date();
//         this._isRunning = true;
//     }
//     Finish()
//     {
//         this._game.reset();
//         this._isRunning = false;
//     }
//     ApplyAdd()
//     {
//         this._game.applyAdd();
//         this.NextRandom();
//     }
//     ApplySub()
//     {
//         this._game.applySub();
//         this.NextRandom();
//     }
//     ApplyMul()
//     {
//         this._game.applyMul();
//         this.NextRandom();
//     }
//     ApplyDiv()
//     {
//         this._game.applyDiv();
//         this.NextRandom();
//     }
//     IsRunning()
//     {
//         return this._isRunning;
//     }
//     IsComplete()
//     {
//         return this._game.getNumber() == this._game.getTarget();
//     }
//     NextRandom()
//     {
//         if (!this._game.nextRandom() && !this.IsComplete())
//         {
//             if (new Date().getSeconds() - this._timer.getSeconds() > 6)
//             {
//                 for (let random = this._randomRange.min; random <= this._randomRange.max; random++)
//                 {
//                     if (random == 0) continue;
//                     if (Math.round(this._game.getNumber() + random) == this._game.getTarget())
//                     return this._game.setRandom(random);
//                     if (Math.round(this._game.getNumber() - random) == this._game.getTarget())
//                     return this._game.setRandom(random);
//                     if (Math.round(this._game.getNumber() * random) == this._game.getTarget())
//                     return this._game.setRandom(random);
//                     if (Math.round(this._game.getNumber() / random) == this._game.getTarget())
//                     return this._game.setRandom(random);
//                 }
//             }
//             this._game.setRandomFromRange(this._randomRange.min, this._randomRange.max);
//             if (this._game.getRandom() == 0) this.NextRandom();
//         }
//     }
//     ResetTimer()
//     {
//         this._timer = new Date();
//     }
//     SetRandomList(values)
//     {
//         this._game.setRandomList(values);
//     }
//     GetNumberValue()
//     {
//         return this._game.getNumber();
//     }
//     GetRandomValue()
//     {
//         return this._game.getRandom();
//     }
//     GetTargetValue()
//     {
//         return this._game.getTarget();
//     }
//     SetNumberRange(min, max)
//     {
//         this._numberRange = { min: min, max: max };
//     }
//     SetRandomRange(min, max)
//     {
//         this._randomRange = { min: min, max: max };
//     }
//     SetTargetRange(min, max)
//     {
//         this._targetRange = { min: min, max: max };
//     }
//     GenerateFromRange(min, max)
//     {
//         return this._game.generateFromRange(min, max);
//     }
// };

/**************************************************************************************************/