/** Dependencies **********************************************************************************/
/** Declarations **********************************************************************************/

export class MenuPage
{
    constructor()
    {
        this.m_elem = document.querySelector('.page.menu');
    }
    DOM() { return this.m_elem; }
    show(callback)
    {
        this.m_elem.className = "page menu";
        if (callback) window.setTimeout(callback, 150);
    }
    hide(callback)
    {
        this.m_elem.className = "page menu hide";
        if (callback) window.setTimeout(callback, 150);
    }
}

/**************************************************************************************************/