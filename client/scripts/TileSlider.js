/** Dependencies **********************************************************************************/

import { EventListeners } from "/imports/classes/EventListeners.js";

/** Declarations **********************************************************************************/

let SINGLETON;

export class TileSlider
{
    constructor()
    {
        // set variables
        this.m_index = 0;
        this.m_values = [];
        this.m_indexes = [];
        this.m_elements = document.querySelectorAll(".layout-page.game .tile-random-list .tile");
        this.m_classNames = [ "tile l2", "tile l1", "tile center", "tile r1", "tile r2" ];
        // prepair & map current objects
        this.Prepair([0]);
        SINGLETON = this;
    }
    Length()
    {
        return this.m_values.length;
    }
    Prepair(values)
    {
        // remove old event listeners
        this._RemoveListeners();
        // set variables
        this.m_index = Math.floor(values.length / 2.0);
        this.m_values = values;
        this.m_indexes = [ 0, 1, 2, 3, 4 ];
        // shuffle value list
        for (let i = 0; i < this.m_values.length; i++)
        {
            let temp = this.m_values[i];
            let index = Math.floor(Math.random() * (this.m_values.length - 1));
            this.m_values[i] = this.m_values[index];
            this.m_values[index] = temp;
        }
        // update current tiles
        this.UpdateTiles();
        // create new event listeners
        this._CreateListeners();
    }
    GetValue()
    {
        return parseInt(this.m_elements[this.m_indexes[2]].innerHTML);
    }
    ShiftLeft()
    {
        // check if valid shift
        if (this.m_index > 0)
            this._ShiftIndicesL();
    }
    ShiftRight()
    {
        // check if valid shift
        if (this.m_index < (this.m_values.length - 1))
            this._ShiftIndicesR();
    }
    RemoveValue()
    {
        // remove element
        this.m_values.splice(this.m_index, 1);
        let centerIndex = this.m_values.length / 2.0;
        // update current tiles
        if (this.m_index == centerIndex)
        {
            console.log("center");   
        }
        else if (this.m_index < centerIndex)
        {
            if (this.m_index-- == 0)
            {
                this._ShiftIndicesR();
            }
        }
        else if (this.m_index > centerIndex)
        {
            if (this.m_index == this.m_values.length)
            {
                this._ShiftIndicesL();
            }
        }
        this.UpdateTiles();
        // final current tiles check
    }
    UpdateTiles()
    {
        let index, value;
        // remove old event listeners
        this._RemoveListeners();
        // update all elements
        for (let i = 0; i < this.m_indexes.length; i++)
        {
            index = this.m_indexes[i];
            value = this.m_values[this.m_index + (i - 2)];
            this.m_elements[index].innerHTML = value == null ? null : value;
            this.m_elements[index].className = this.m_classNames[i] + (value == null ? " hidden" : "");
        }
        // create new event listeners
        this._CreateListeners();
    }
    _ShiftIndicesL(endIndex = this.m_elements.length - 1)
    {
        // update indexes
        this.m_index--;
        for (let i = 0; i < this.m_indexes.length; i++)
            this.m_indexes[i] = (this.m_indexes[i] == 0 ? endIndex : this.m_indexes[i] - 1);
    }
    _ShiftIndicesR(endIndex = this.m_elements.length - 1)
    {
        // update indexes
        this.m_index++;
        for (let i = 0; i < this.m_indexes.length; i++)
            this.m_indexes[i] = (this.m_indexes[i] == endIndex ? 0 : this.m_indexes[i] + 1);
    }
    _RemoveListeners()
    {
        EventListeners.RemoveAll(this.m_elements[0]);
        EventListeners.RemoveAll(this.m_elements[1]);
        EventListeners.RemoveAll(this.m_elements[2]);
        EventListeners.RemoveAll(this.m_elements[3]);
        EventListeners.RemoveAll(this.m_elements[4]);
    }
    _CreateListeners()
    {
        if (this.m_elements[this.m_indexes[1]].className.search("hidden") == -1)
        {
            EventListeners.Add(this.m_elements[this.m_indexes[1]], "touchstart", function(e) {
                SINGLETON.ShiftLeft();
                SINGLETON.UpdateTiles();
            }, { passive: true });
        }
        if (this.m_elements[this.m_indexes[3]].className.search("hidden") == -1)
        {
            EventListeners.Add(this.m_elements[this.m_indexes[3]], "touchstart", function(e) {
                SINGLETON.ShiftRight();
                SINGLETON.UpdateTiles();
            }, { passive: true });
        }   
    }
}

/**************************************************************************************************/